(function() {
  'use strict';

  angular
    .module('tntAssessmentApp')
    .controller('SignUpCtrl', SignUpCtrl);
    
  SignUpCtrl.$inject = ['SignUpApiService', 'SignUpValidationService'];
  function SignUpCtrl(SignUpApiService, SignUpValidationService) {
    var self = this;
    self.loading = false;
    self.isUserRegistered = false;

    self.signUpUser = function () {
      self.errorMessage = null;
      self.hasServerError = false;
      self.isUserRegistered = false;

      var isEmailValid = self._isEmailValid(self.user.email);
      var isPasswordValid = self._isPasswordValid(self.user.password, self.user.passwordCheck); 

      if(isEmailValid && isPasswordValid) {
        self.loading = true;
      
        SignUpApiService
          .createUser({email: self.user.email, password: self.user.password})
          .then(function(resp) {
            self.loading = false;
            self.isUserRegistered = true;
          }, function(error) {
            self.loading = false;
            self.hasServerError = true;
          });
      }
    };

    self._isEmailValid = function(email) {
      var emailRegex = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
      if(emailRegex.test(email) === false){
        self.errorMessage = 'The email is not valid';
        return false;
      }
      return true;
    };

    self._isPasswordValid = function(password, passwordCheck) {
      if(SignUpValidationService.hasDifferentPasswords(password, passwordCheck)) {
        self.errorMessage = 'The passwords does not match';
        return false;
      }
      if(SignUpValidationService.hasIncorrectSize(password)) {
        self.errorMessage = 'The password contains more than 32 characters';
        return false;
      }
      if(!SignUpValidationService.hasOnlyAlphabeticLetters(password)) {
        self.errorMessage = 'Password can not contain numbers, symbols or upper case letters';
        return false;
      }
      if(SignUpValidationService.hasInsufficientOverlappingChars(password)) {
        self.errorMessage = 'Passwords must contain at least two non-overlapping pairs of letters, like \'aa\', \'bb\' or \'zz\'';
        return false;
      }
      if(SignUpValidationService.hasInvalidLetters(password)) {
        self.errorMessage = 'Password can not contains the letters i, o or l';
        return false;
      }
      if(SignUpValidationService.hasOneIncreasingCharOrder(password) === false) {
        self.errorMessage = 'Passwords must include one increasing straight of at least three letters, like \'abc\', \'bcd\', and so on, up to \'xyz\'';
        return false;
      }

      return true;
    }; 
  }

})();