(function() {
  'use strict';

  angular
    .module('tntAssessmentApp')
    .service('SignUpValidationService', SignUpValidationService);

  function SignUpValidationService() {

    this.hasOneIncreasingCharOrder = function(password) {
      var sequenceSize = 1;
      var count = 0;
      for(var i = 0; i < password.length; i++) {
        if(password.charCodeAt(i) + 1 === password.charCodeAt(i + 1)) {
          sequenceSize++;
          if(sequenceSize === 3) {
            count++;
          }
        } else {
          sequenceSize = 1;
        }
      }
      return count === 1;
    };

    this.hasDifferentPasswords = function(password, passwordCheck) {
      return password !== passwordCheck;
    };
      
    this.hasInvalidLetters = function(password) {
      var regex = new RegExp(/(i|l|o)/g);
      return password.match(regex) !== null;
    };

    this.hasInsufficientOverlappingChars = function(password) {
      var regex = new RegExp(/([a-z])\1{1,}/g);
      var found = password.match(regex);
      return found === null || found.length < 2;
    };

    this.hasIncorrectSize = function(password) {
      return password.length > 32;
    };

    this.hasOnlyAlphabeticLetters = function(password) {
      var regex = new RegExp(/[a-z]/g);
      var found = password.match(regex);
      return found !== null && found.length === password.length;
    };

  }
      
})();