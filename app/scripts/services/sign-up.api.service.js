(function() {
  'use strict';

  angular
    .module('tntAssessmentApp')
    .service('SignUpApiService', SignUpApiService);

    SignUpApiService.$inject = ['$http'];
    function SignUpApiService($http) {

      this.createUser = function(user) {
        return $http.post('/register', user);
      };

    }

})();