(function() {
  'use strict';

  angular
    .module('tntAssessmentAppDev', [
      'tntAssessmentApp', 'ngMockE2E'
    ])

    /* Generate 2000ms delay for each request */
    .config(function($provide) {
      $provide.decorator('$httpBackend', function($delegate) {
          var proxy = function(method, url, data, callback, headers) {
              var interceptor = function() {
                  var _this = this,
                      _arguments = arguments;
                  setTimeout(function() {
                      callback.apply(_this, _arguments);
                  }, 2000);
              };
              return $delegate.call(this, method, url, data, interceptor, headers);
          };
          for(var key in $delegate) {
              proxy[key] = $delegate[key];
          }
          return proxy;
      });
    })
    
    /* Mock backend API */
    .run(function($httpBackend) {
      $httpBackend.whenPOST('/register').respond(function(method, url, data){
        return [201];
      });

      $httpBackend.whenGET(/\.html$/).passThrough();
    });
  
})();