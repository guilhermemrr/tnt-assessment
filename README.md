## TNT Assessment
This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Installing dependencies
Run `bower install` and `npm install` to install dependencies.

## Build & Development
Run `grunt build` for building and `grunt serve` for development.

## Testing
Running `grunt test` will run the unit tests with karma.
