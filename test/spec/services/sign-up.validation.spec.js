'use strict';

describe('Service: SignUpValidation', function() {
  var SignUpValidationService;

  beforeEach(module('tntAssessmentApp'));

  beforeEach(inject(function (_SignUpValidationService_) {
    SignUpValidationService = _SignUpValidationService_;
  }));

  describe('hasIncorrectSize()', function() {
    
    it('should return true when password has incorrect size', function() {
      var password = 'abcabcabcabcabcabcabcabcabcabcabcabcabcabc';
      var hasIncorrectSize = SignUpValidationService.hasIncorrectSize(password);
      expect(hasIncorrectSize).toBe(true);
    });

    it('should return false when password has correct size', function() {
      var password = 'abcabcabc';
      var hasIncorrectSize = SignUpValidationService.hasIncorrectSize(password);
      expect(hasIncorrectSize).toBe(false);
    });

  });

  describe('hasOnlyAlphabeticLetters()', function() {

    it('should return true when it has lower case letters', function(){
      var password = 'abcdef';
      var hasOnlyAlphabeticLetters = SignUpValidationService.hasOnlyAlphabeticLetters(password);
      expect(hasOnlyAlphabeticLetters).toBe(true);
    });

    it('should return false when it has numbers, symbols or upper case letters', function(){
      var password, hasOnlyAlphabeticLetters; 
    
      password = 'abc123';
      hasOnlyAlphabeticLetters = SignUpValidationService.hasOnlyAlphabeticLetters(password);
      expect(hasOnlyAlphabeticLetters).toBe(false);

      password = 'abcdEFg';
      hasOnlyAlphabeticLetters = SignUpValidationService.hasOnlyAlphabeticLetters(password);
      expect(hasOnlyAlphabeticLetters).toBe(false);

      password = 'abc--&x';
      hasOnlyAlphabeticLetters = SignUpValidationService.hasOnlyAlphabeticLetters(password);
      expect(hasOnlyAlphabeticLetters).toBe(false);
    });

  });

  describe('hasInsufficientOverlappingChars()', function(){

    it('should return false when it has two or more pair of overlapping chars', function() {
      var password = 'aabcc';
      var hasInsufficientOverlappingChars = SignUpValidationService.hasInsufficientOverlappingChars(password);
      expect(hasInsufficientOverlappingChars).toBe(false);  
    });

    it('should return true when it has less than two pair of overlapping chars', function() {
      var password, hasInsufficientOverlappingChars;

      password = 'aabc';
      hasInsufficientOverlappingChars = SignUpValidationService.hasInsufficientOverlappingChars(password);
      expect(hasInsufficientOverlappingChars).toBe(true);  

      password = 'abc';
      hasInsufficientOverlappingChars = SignUpValidationService.hasInsufficientOverlappingChars(password);
      expect(hasInsufficientOverlappingChars).toBe(true);  

      password = 'cccc';
      hasInsufficientOverlappingChars = SignUpValidationService.hasInsufficientOverlappingChars(password);
      expect(hasInsufficientOverlappingChars).toBe(true);  

    });
    
  });

  describe('hasInvalidLetters()', function(){
    
    it('should return true when password has i,o, or l', function(){
      var password, hasInvalidLetters;

      password = 'abcicc';
      hasInvalidLetters = SignUpValidationService.hasInvalidLetters(password);
      expect(hasInvalidLetters).toBe(true);

      password = 'abcox';
      hasInvalidLetters = SignUpValidationService.hasInvalidLetters(password);
      expect(hasInvalidLetters).toBe(true);

      password = 'abclcc';
      hasInvalidLetters = SignUpValidationService.hasInvalidLetters(password);
      expect(hasInvalidLetters).toBe(true);

      password = 'iolaaaac';
      hasInvalidLetters = SignUpValidationService.hasInvalidLetters(password);
      expect(hasInvalidLetters).toBe(true);
    });

    it('should return false when password has valid letters', function() {
      var password, hasInvalidLetters;

      password = 'abccpd';
      hasInvalidLetters = SignUpValidationService.hasInvalidLetters(password);
      expect(hasInvalidLetters).toBe(false);
    });

  });

  describe('hasDifferentPasswords()', function(){
    it('should return true if the passwords are different', function() {
      var password1 = 'zzbbaa';    
      var password2 = 'aabbcc';

      var hasDifferentPasswords = SignUpValidationService.hasDifferentPasswords(password1, password2);
      expect(hasDifferentPasswords).toBe(true);
    });

    it('should return false if the passwords are equal', function() {
      var password1 = 'aabbcc';    
      var password2 = 'aabbcc';

      var hasDifferentPasswords = SignUpValidationService.hasDifferentPasswords(password1, password2);
      expect(hasDifferentPasswords).toBe(false);
    });

  });

  describe('hasOneIncreasingCharOrder', function() {

    it('should return true when the password contain only one increasing char order', function(){
      var password = 'bhdjabc';
      var hasOneIncreasingCharOrder = SignUpValidationService.hasOneIncreasingCharOrder(password);
      expect(hasOneIncreasingCharOrder).toBe(true);
    });

    it('should return false when the password doesnt contains or contains more than one increasing char order', function(){
      var password, hasOneIncreasingCharOrder;

      password = 'acbhgtk';
      hasOneIncreasingCharOrder = SignUpValidationService.hasOneIncreasingCharOrder(password);
      expect(hasOneIncreasingCharOrder).toBe(false);

      password = 'abcabcdeghj';
      hasOneIncreasingCharOrder = SignUpValidationService.hasOneIncreasingCharOrder(password);
      expect(hasOneIncreasingCharOrder).toBe(false);
    });

  });  

});