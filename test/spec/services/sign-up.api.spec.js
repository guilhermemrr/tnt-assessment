'use strict';

describe('Service: SignUpApi', function () {
  var SignUpApiService, httpBackend, newUser;

  beforeEach(module('tntAssessmentApp'));

  beforeEach(inject(function (_SignUpApiService_, $httpBackend) {
    SignUpApiService = _SignUpApiService_;
    httpBackend = $httpBackend;
    newUser = { email: 'user@gmail.com', password: 'aabcc' };
  }));

  afterEach(function() {
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
  });

  it('should register user with success', function () {
    httpBackend.whenPOST('/register', newUser).respond(201);

    SignUpApiService.createUser(newUser).then(function(response) {
      expect(response.status).toBe(201);
    });
    httpBackend.flush();
  });

  it('should fail with some internal server error', function () {
    httpBackend.whenPOST('/register', newUser).respond(500, '');

    SignUpApiService
      .createUser(newUser)
      .then(function() {
      })
      .catch(function(error){
        expect(error.status).toBe(500);
      });
    
    httpBackend.flush();
  });

});
