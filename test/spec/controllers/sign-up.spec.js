'use strict';

describe('Controller: SignUp', function() {

  beforeEach(module('tntAssessmentApp'));

  describe('signUpUser()', function() {
    var SignUpCtrl, SignUpApiService, SignUpValidationService, rootScope, passPromise, password, email;

    beforeEach(inject(function($q, $rootScope, $controller, _SignUpValidationService_, _SignUpApiService_){
      rootScope = $rootScope;

      /* Mock email verification */
      SignUpValidationService = _SignUpValidationService_;
      SignUpValidationService.isEmailValid = jasmine.createSpy().and.returnValue(true);

      /* Mock createUser promise */
      SignUpApiService = _SignUpApiService_;
      SignUpApiService.createUser = jasmine.createSpy().and.callFake(function(){
        var deferred = $q.defer();
        if(passPromise) {
          deferred.resolve();
        } else {
          deferred.reject();
        }
        return deferred.promise;
      });

      /* Create controller with mocked data */
      SignUpCtrl = $controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService: SignUpValidationService
      });

      /* User info */
      password = 'aabcc';
      email = 'test@gmail.com';

      SignUpCtrl.user = {
        email: email,
        password: password,
        passwordCheck: password
      };

      /* Mock method inside the controller */
      SignUpCtrl._isPasswordValid = jasmine.createSpy().and.returnValue(true);
      SignUpCtrl._isEmailValid = jasmine.createSpy().and.returnValue(true);


    }));

    it('should disable button after submiting', function(){
        SignUpCtrl.signUpUser();
        expect(SignUpCtrl.loading).toBe(true);
    });

    it('should register user and show message', function() {
      passPromise = true;

      SignUpCtrl.signUpUser();
      rootScope.$digest();

      expect(SignUpCtrl.isUserRegistered).toBe(true);
      expect(SignUpCtrl.loading).toBe(false);
      expect(SignUpCtrl._isPasswordValid).toHaveBeenCalledWith(password, password);
      expect(SignUpCtrl._isEmailValid).toHaveBeenCalledWith(email);
      expect(SignUpApiService.createUser).toHaveBeenCalledWith({email: email, password: password});
    });

    it('should get server side error and show message', function() {
      passPromise = false;
     
      SignUpCtrl.signUpUser();
      rootScope.$digest();

      expect(SignUpCtrl.hasServerError).toBe(true);
      expect(SignUpCtrl.isUserRegistered).toBe(false);
      expect(SignUpCtrl.loading).toBe(false);
      expect(SignUpCtrl._isPasswordValid).toHaveBeenCalledWith(password, password);
      expect(SignUpCtrl._isEmailValid).toHaveBeenCalledWith(email);
      expect(SignUpApiService.createUser).toHaveBeenCalledWith({email: email, password: password});
    });

  });

   describe('_isEmailValid()', function() {
    var SignUpCtrl, SignUpValidationService, SignUpApiService, controller;

    beforeEach(inject(function($controller, _SignUpValidationService_, _SignUpApiService_){
      SignUpValidationService = _SignUpValidationService_;
      SignUpApiService = _SignUpApiService_;
      
      SignUpCtrl = $controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService: SignUpValidationService
      });
    }));


    it('should return true when the email is valid', function() {
      var email = 'test@gmail.com';
      var isValid = SignUpCtrl._isEmailValid(email);
      expect(isValid).toBe(true);
    });

    it('should return false and show error when the email is invalid', function() {
      var email, isValid;
      
      email = 'test@gmail.';
      isValid = SignUpCtrl._isEmailValid(email);
      expect(SignUpCtrl.errorMessage).toBe('The email is not valid')
      expect(isValid).toBe(false);

      SignUpCtrl.errorMessage = null;

      email = '@gmail.c';
      isValid = SignUpCtrl._isEmailValid(email);
      expect(SignUpCtrl.errorMessage).toBe('The email is not valid')
      expect(isValid).toBe(false);

      SignUpCtrl.errorMessage = null;

      email = 'test@gmail.c';
      isValid = SignUpCtrl._isEmailValid(email);
      expect(SignUpCtrl.errorMessage).toBe('The email is not valid')
      expect(isValid).toBe(false);

      SignUpCtrl.errorMessage = null;

      email = 'testgmail.c';
      isValid = SignUpCtrl._isEmailValid(email);
      expect(SignUpCtrl.errorMessage).toBe('The email is not valid')
      expect(isValid).toBe(false);
    });

  });


  describe('_isPasswordValid()', function() {
    var SignUpCtrl, SignUpValidationService, SignUpApiService, controller;

    beforeEach(inject(function($controller, _SignUpValidationService_, _SignUpApiService_){
      SignUpValidationService = _SignUpValidationService_;
      SignUpApiService = _SignUpApiService_;
      controller = $controller;

      /* Mock all methods of SignUpValidationService to validate the user with success */
      SignUpValidationService.hasDifferentPasswords = jasmine.createSpy().and.returnValue(false);
      SignUpValidationService.hasIncorrectSize = jasmine.createSpy().and.returnValue(false);
      SignUpValidationService.hasOnlyAlphabeticLetters = jasmine.createSpy().and.returnValue(true);
      SignUpValidationService.hasInsufficientOverlappingChars = jasmine.createSpy().and.returnValue(false);
      SignUpValidationService.hasInvalidLetters = jasmine.createSpy().and.returnValue(false);
      SignUpValidationService.hasOneIncreasingCharOrder = jasmine.createSpy().and.returnValue(true);
    }));

    it('should return false and show error when passwords are different', function() {
      /* Override hasDifferentPasswords mock to fail */
      SignUpValidationService.hasDifferentPasswords = jasmine.createSpy().and.returnValue(true);

      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'aabcc';
      var password2 = 'bbccaa';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(isValid).toEqual(false);
      expect(SignUpCtrl.errorMessage).toBe('The passwords does not match');
    });

    it('should return false and show error when password has incorrect size', function() {
      /* Override hasIncorrectSize to fail */
      SignUpValidationService.hasIncorrectSize = jasmine.createSpy().and.returnValue(true);

      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'aabccaabccaabccaabccaabccaabccaabccaabccaabcc';
      var password2 = 'aabccaabccaabccaabccaabccaabccaabccaabccaabcc';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(isValid).toEqual(false);
      expect(SignUpCtrl.errorMessage).toBe('The password contains more than 32 characters');
    });

    it('should return false and show error when password has something different than lower case letters', function() {
      /* Override hasOnlyAlphabeticLetters to fail */
      SignUpValidationService.hasOnlyAlphabeticLetters = jasmine.createSpy().and.returnValue(false);

      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'abc24031992';
      var password2 = 'abc24031992';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(isValid).toEqual(false);
      expect(SignUpCtrl.errorMessage).toBe('Password can not contain numbers, symbols or upper case letters');
    });

    it('should return false and show error when password has insufficient overlapping chars', function() {
      /* Override hasInsufficientOverlappingChars to fail */
      SignUpValidationService.hasInsufficientOverlappingChars = jasmine.createSpy().and.returnValue(true);

      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'aabc';
      var password2 = 'aabc';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(isValid).toEqual(false);
      expect(SignUpCtrl.errorMessage).toBe('Passwords must contain at least two non-overlapping pairs of letters, like \'aa\', \'bb\' or \'zz\'');
    });


    it('should return false and show error when password has invalid chars', function() {
      /* Override hasInvalidLetters to fail */
      SignUpValidationService.hasInvalidLetters = jasmine.createSpy().and.returnValue(true);

      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'aabicl';
      var password2 = 'aabicl';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(isValid).toEqual(false);
      expect(SignUpCtrl.errorMessage).toBe('Password can not contains the letters i, o or l');
    });

    it('should return false and show error when password has zero or more than one increasing char order', function() {
      /* Override hasOneIncreasingCharOrder to fail */
      SignUpValidationService.hasOneIncreasingCharOrder = jasmine.createSpy().and.returnValue(false);

      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'aabdcc';
      var password2 = 'aabdcc';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(isValid).toEqual(false);
      expect(SignUpCtrl.errorMessage).toBe('Passwords must include one increasing straight of at least three letters, like \'abc\', \'bcd\', and so on, up to \'xyz\'');
    });

    it('should return true when password is valid', function() {
      SignUpCtrl = controller('SignUpCtrl', {
        SignUpApiService: SignUpApiService,
        SignUpValidationService:SignUpValidationService
      });

      var password1 = 'aabcc';
      var password2 = 'aabcc';

      var isValid =  SignUpCtrl._isPasswordValid(password1, password2);
      expect(SignUpValidationService.hasDifferentPasswords).toHaveBeenCalled();
      expect(SignUpValidationService.hasIncorrectSize).toHaveBeenCalled();
      expect(SignUpValidationService.hasOnlyAlphabeticLetters).toHaveBeenCalled();
      expect(SignUpValidationService.hasInsufficientOverlappingChars).toHaveBeenCalled();
      expect(SignUpValidationService.hasInvalidLetters).toHaveBeenCalled();
      expect(SignUpValidationService.hasOneIncreasingCharOrder).toHaveBeenCalled();
      expect(isValid).toEqual(true);
    });

  });


});